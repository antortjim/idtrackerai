 <!-- Thank you for reporting an issue -->
#### Steps to reproduce the issue



#### Describe the current behavior



#### Describe the expected behavior



#### Technical details:

- Operating system version: 
- Python version:
- CUDA/cuDNN version:
- GPU model and memory:
- idtracker.ai version: 
- How did you install idtracker.ai?

#### Logs/reports
<!-- Please, include any logs, code or exception tracebacks that could be helpful for us to diagnose the problem. Large logs and files should be attached. -->