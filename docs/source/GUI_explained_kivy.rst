Graphical user interface (GUI)
==============================

idtracker.ai graphical user interface is organized in tabs that activate and
deactivate along the tracking process in order to guide the user.

^^^^^^^^^^^^^^^
Tab 1. Welcome
^^^^^^^^^^^^^^^

.. figure:: ./_static/kivy/gui_images.001.png
   :scale: 80 %
   :align: center
   :alt: welcome tab 1

.. figure:: ./_static/kivy/gui_images.002.png
  :scale: 80 %
  :align: center
  :alt: welcome tab 2


^^^^^^^^^^^^^^^^^^^^^
Tab 2. ROI selection
^^^^^^^^^^^^^^^^^^^^^

.. figure:: ./_static/kivy/gui_images.003.png
   :scale: 80 %
   :align: center
   :alt: ROI selection

^^^^^^^^^^^^^^^^^^^^
Tab 3. Preprocessing
^^^^^^^^^^^^^^^^^^^^

.. figure:: ./_static/kivy/gui_images.004.png
  :scale: 80 %
  :align: center
  :alt: preprocessing 1

.. figure:: ./_static/kivy/gui_images.005.png
  :scale: 80 %
  :align: center
  :alt: preprocessing 2

^^^^^^^^^^^^^^^
Tab 4. Tracking
^^^^^^^^^^^^^^^

.. figure:: ./_static/kivy/gui_images.006.png
  :scale: 80 %
  :align: center
  :alt: tracking 1

.. figure:: ./_static/kivy/gui_images.007.png
  :scale: 80 %
  :align: center
  :alt: tracking 2

^^^^^^^^^^^^^^^^^
Tab 5. Validation
^^^^^^^^^^^^^^^^^

.. figure:: ./_static/kivy/gui_images.008.png
  :scale: 80 %
  :align: center
  :alt: validation
